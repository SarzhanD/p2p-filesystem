package file.system.handler;

import file.system.node.Node;

import java.io.*;
import java.net.Socket;

public class NodeRequestHandler extends AbstractRequestHandler implements Runnable {

    private Node node;

    public NodeRequestHandler(Socket socket, Node node) {
        super(socket);
        this.node = node;
    }

    public void handle() {
        try (
                InputStream sin = socket.getInputStream();
                OutputStream sout = socket.getOutputStream();
                DataInputStream in = new DataInputStream(sin);
                DataOutputStream out = new DataOutputStream(sout)
        ) {
            String request;

            request = in.readUTF();
            if (request.startsWith("key:")) {
                /*
                    В ноду еще нужно добавить Мапу(ключ = уникальный генерируемый хеш, значение - ДТО с самими данными и ид ноды с которой пришли).
                    Тут надо ловить ид ноды с которой пришел запрос и если файл, найденный по ключу не от этой ноды, то ничего не шлем.
                 */
            } else {
            }

        } catch (IOException ignored) {

        }
    }

    public void run() {
        handle();
    }
}
