package file.system.handler;

import com.google.gson.Gson;
import file.system.AddressHolderServer;
import file.system.util.NodeIdGenerator;

import java.io.*;
import java.net.Socket;
import java.util.Map;

public class AddressServerRequestHandler extends AbstractRequestHandler implements Runnable {

    private AddressHolderServer addressHolderServer;

    public AddressServerRequestHandler(Socket socket, AddressHolderServer addressHolderServer) {
        super(socket);
        this.addressHolderServer = addressHolderServer;
    }

    public void run() {
        handle();
    }

    public void handle() {
        try (
                InputStream sin = socket.getInputStream();
                OutputStream sout = socket.getOutputStream();
                DataInputStream in = new DataInputStream(sin);
                DataOutputStream out = new DataOutputStream(sout);
        ) {

            String clientAddress = socket.getRemoteSocketAddress().toString();
            String request;

            request = in.readUTF();
            if (request.startsWith("register node")) {
                String newNodeId = NodeIdGenerator.generate();
                addressHolderServer.addNewNode(newNodeId, clientAddress);
                out.writeUTF(newNodeId);
                out.flush();
            } else if (request.startsWith("get nodes")) {
                Map<String, String> nodeIdList = addressHolderServer.getAvailableNodes();
                Gson gson = new Gson();
                out.writeUTF(gson.toJson(nodeIdList));
            }

        } catch (IOException ignored) {

        }
    }
}
