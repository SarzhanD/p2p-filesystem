package file.system.handler;

import java.net.Socket;

public abstract class AbstractRequestHandler implements RequestHandler {

    protected Socket socket;

    public AbstractRequestHandler(Socket socket) {
        this.socket = socket;
    }
}
