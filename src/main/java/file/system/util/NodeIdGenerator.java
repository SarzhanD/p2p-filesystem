package file.system.util;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.Random;

/**
 * Created by Dimas on 25.07.2018.
 */
public class NodeIdGenerator {

        private static final Random random = new Random();

        public static String generate(){
            return DigestUtils.sha256Hex(Long.valueOf(random.nextLong()).toString());
        }
}
