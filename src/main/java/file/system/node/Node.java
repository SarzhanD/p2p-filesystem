package file.system.node;

import com.google.gson.Gson;
import file.system.AddressHolderServer;
import file.system.handler.NodeRequestHandler;
import file.system.metadata.AddressHolder;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Node extends AbstractNode {

    private String id;

    private int addressHolderServerPort;
    private String addressHolderServerHost;


    public Node() {
        AddressHolder addressHolder = AddressHolderServer.class.getAnnotation(AddressHolder.class);
        addressHolderServerHost = addressHolder.host();
        addressHolderServerPort = addressHolder.port();
    }

    private void registerNode() throws IOException {
        InetAddress ipAddress = InetAddress.getByName(addressHolderServerHost);
        Socket socket = new Socket(ipAddress, addressHolderServerPort);

        OutputStream sout = socket.getOutputStream();
        InputStream sin = socket.getInputStream();
        DataOutputStream out = new DataOutputStream(sout);
        DataInputStream in = new DataInputStream(sin);

        out.writeUTF("register node");
        out.flush();

        id = in.readUTF();
    }

    @SuppressWarnings("unchecked")
    private void loadAvailableNodes() throws IOException {
        InetAddress ipAddress = InetAddress.getByName(addressHolderServerHost);
        Socket socket = new Socket(ipAddress, addressHolderServerPort);

        InputStream sin = socket.getInputStream();
        OutputStream sout = socket.getOutputStream();
        DataInputStream in = new DataInputStream(sin);
        DataOutputStream out = new DataOutputStream(sout);

        String request = "get nodes";
        out.writeUTF(request);
        out.flush();
        String json = in.readUTF();
        nodeIdList = ((Map<String, String>) new Gson().fromJson(json, Map.class));
    }

    public Map<String, String> getAvailableNodes() {
        return nodeIdList;
    }

    public String getRandomNode() {
        Random random = new Random();
        List<String> keys = new ArrayList<>(nodeIdList.keySet());
        String randomKey = keys.get(random.nextInt(keys.size()));
        String value = nodeIdList.get(randomKey);
        if (value.equals(id)){
            value = getRandomNode();
        }
        return value;
    }

    public void run() throws IOException {
        ServerSocket ss = new ServerSocket(nodePort);
        nodeAddress = ss.getInetAddress().getHostAddress();
        registerNode();
        loadAvailableNodes();
        while (true) {
            try(BufferedReader br =new BufferedReader(new InputStreamReader(System.in))){
                if (br.ready()){
                    String message = br.readLine();
                    /*
                        Сдеалать проверку это на получение или на отправку.
                        Если отправка, то ищем достаем адресс рандомной ноды, шлем данные, получаем хеш сообщения и записываем куда послали.
                        Если получение - вводим хеш, устанавливаем соединение с нодой, куда мы слали.
                     */
                } else {
                    Socket socket = ss.accept();
                    executorService.execute(new NodeRequestHandler(socket, this));
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        new Node().run();
    }
}
