package file.system.node;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class AbstractNode {

    protected ExecutorService executorService = Executors.newFixedThreadPool(5);

    protected Map<String, String> nodeIdList;

    protected String nodeAddress;

    protected int nodePort;
}
