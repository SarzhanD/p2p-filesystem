package file.system;

import file.system.node.AbstractNode;
import file.system.handler.AddressServerRequestHandler;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.Map;

import file.system.metadata.AddressHolder;

@AddressHolder(host = "127.0.0.1", port = 6666)
public class AddressHolderServer extends AbstractNode {


    public  AddressHolderServer(){
        AddressHolder addressHolder = AddressHolderServer.class.getAnnotation(AddressHolder.class);
        nodePort = addressHolder.port();
    }

    public void addNewNode(String newNodeId, String clientAddress) {
        nodeIdList.put(newNodeId, clientAddress);
    }

    public Map<String, String> getAvailableNodes(){
        return nodeIdList;
    }

    private void run() throws IOException {
        ServerSocket ss = new ServerSocket(nodePort);
        while (true){
            Socket socket = ss.accept();
            executorService.execute(new AddressServerRequestHandler(socket, this));
        }

    }

    public static void main(String[] args) throws IOException {
        new AddressHolderServer().run();
    }

}
